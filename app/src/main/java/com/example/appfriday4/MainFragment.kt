package com.example.appfriday4

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appfriday4.databinding.MainFragmentBinding
import kotlinx.coroutines.launch

class MainFragment : Fragment() {

    private val viewModel: FragmentViewModel by viewModels()
    private lateinit var binding: MainFragmentBinding
    private lateinit var myAdapter: RecyclerMainAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        viewModel.init()
        initRecycler()
        observe()
    }


    private fun initRecycler() {
        myAdapter = RecyclerMainAdapter()
        binding.recycler.layoutManager = LinearLayoutManager(requireActivity())
        binding.recycler.adapter = myAdapter
    }

    private fun observe() {

        viewModel._jsonLiveData.observe(viewLifecycleOwner, {
            myAdapter.setData(it.toMutableList())
        })
    }


}