package com.example.appfriday4

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofitStuff.RetrofitService

class FragmentViewModel : ViewModel() {
    private val jsonLiveData = MutableLiveData<List<List<JsonModel>>>().apply {
        mutableListOf<List<JsonModel>>()
    }

    val _jsonLiveData: LiveData<List<List<JsonModel>>> = jsonLiveData

    fun init() {
        CoroutineScope(Dispatchers.IO).launch {
            getCountries()
        }
    }


    private suspend fun getCountries() {
        val result = RetrofitService.service().getJson()

        if (result.isSuccessful) {
            val items = result.body()
            jsonLiveData.postValue(items)
        }
    }
}