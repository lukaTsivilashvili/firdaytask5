package com.example.appfriday4

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday4.databinding.ActivityMainBinding
import com.example.appfriday4.databinding.MainFragmentBinding

class RecyclerMainAdapter(): RecyclerView.Adapter<RecyclerMainAdapter.MainViewHolder>() {
    val jsonList = mutableListOf<List<JsonModel>>()

    inner class MainViewHolder(private val binding:MainFragmentBinding):RecyclerView.ViewHolder(binding.root){



    }

    fun setData(jsonList: MutableList<List<JsonModel>>){
        this.jsonList.addAll(jsonList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val itemView =
            MainFragmentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = MainViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

    }

    override fun getItemCount() = jsonList.size

}