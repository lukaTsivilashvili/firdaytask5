package com.example.appfriday4

import com.google.gson.annotations.SerializedName

data class JsonModel(

    @SerializedName("field_id")
    val fieldId: Int,
    val hint: String,

    @SerializedName("field_type")
    val fieldtType: String,
    val keyboard: String,
    val required: Boolean,

    @SerializedName("is_active")
    val isActive: Boolean,
    val icon: String

)
