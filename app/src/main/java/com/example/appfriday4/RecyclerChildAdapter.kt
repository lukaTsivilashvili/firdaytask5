package com.example.appfriday4

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday4.databinding.ItemMainRecyclerBinding
import com.example.appfriday4.databinding.RecyclercardviewBinding

class RecyclerChildAdapter(private val childList:List<JsonModel>): RecyclerView.Adapter<RecyclerChildAdapter.ChildViewHolder>() {

    inner class ChildViewHolder(private val binding:ItemMainRecyclerBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ChildViewHolder {
        val itemView =
            ItemMainRecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ChildViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerChildAdapter.ChildViewHolder, position: Int) {

    }

    override fun getItemCount() =childList.size
}